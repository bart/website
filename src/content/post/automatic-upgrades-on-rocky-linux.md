---
publishDate: 2024-02-22T12:00:00Z
title: Automatic Upgrades on Rocky Linux
excerpt: Use this guide to configure automatic upgrades on Rocky Linux.
tags:
  - linux
  - rocky
  - dnf-automatic
---

For every one of my servers, I configure automated security updates. This removes some of the overhead that is included when maintaining servers.

## Configuring Automatic Upgrades

Rocky provides a tool called `dnf-automatic` to automatically retrieve and install security patches and other essential upgrades for your server. 

If this tool is not installed already, you can install it with the following command.

```bash
sudo dnf install dnf-automatic -y
```

You can now configure the tool by changing the configuration file. Open this file using the editor.

```bash
sudo vi /etc/dnf/automatic.conf
```

We need to enable automatic updates by setting the `apply_updates` option to `yes`. Otherwise, `dnf-automatic` will only download but not install the available updates.

```
[commands]
...
apply_updates = yes
...
```

I choose to only enable security patches to be automatically installed on my system. I don't want all packages to be updated, this could lead to unexpected changes in functionality. 

We do this by changing the value of the `upgrade_type` option to `security`.

```
[commands]
...
upgrade_type = security
...
```

Finally, you need to enable the service.

```bash
sudo systemctl enable dnf-automatic.timer
```

You can check the status of the automatic updates process.

```bash
sudo systemctl list-timers dnf-*
```

## Configuring the Timer

You can choose to change the configuration of the timer by using the command below.

```bash
sudo systemctl edit dnf-automatic.timer
```

If you made changes to the configuration file, restart the `dnf-automatic` timer in order for the changes to take effect.

```bash
sudo systemctl restart dnf-automatic.timer
```

