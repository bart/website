---
publishDate: 2024-02-22T16:00:00Z
title: Automating Mailcow backups 
excerpt: Mailcow allows you to easily host your own mail server using Docker. This is how I back up my mail server.
tags:
  - self-hosting
  - linux
  - mailcow
---

Mailcow allows you to easily host your own mail server using Docker. Below are the scripts and steps I took to back up my mail server using rclone.

For the scripts to work, you first need to install `rclone` using the following command.

```bash
curl https://rclone.org/install.sh | sudo bash
```

You now need to set up your rclone remote.

```bash
rclone config
```

Next, you have to download the backup script from the repository. This script will be executed using a cron job.

```bash
curl https://codeberg.org/bart/backup-scripts/raw/branch/master/mailcow-backup.sh -o backup.sh
```

This file also contains the environment variables used to configure the backup script. Change the environment variables at the top of the file for the script to fit your backup plan.

```bash
sudo vi backup.sh
```

Finally, we need to add the backup script to the `/etc/crontab` file. For me, the backup script runs once a day at 03:00 AM.

```
0 3 * * * root /root/backup.sh
```

