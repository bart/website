---
publishDate: 2023-11-19T00:00:00Z
title: Dynamic DNS with ddclient on Raspberry Pi
excerpt: Dynamic DNS allows for a record to be automatically updated. These are the steps on how to configure ddclient on your Raspberry Pi. 
tags:
  - raspberrypi
  - ddclient
  - cloudflare
---

Currently, all my domains are managed using Cloudflare. I wanted to use `ddclient` to update the IP addresses of my domains. These are the steps I followed.

If not already installed, you can install it with the following command.

```bash
sudo apt install ddclient
```

You can now change the configuration by changing the configuration file. Open this file using the editor.

```bash
sudo vim /etc/ddclient.conf
```

The configuration for Cloudflare DNS should look something like this:protocol=cloudflare 

```
use=web, web=https://api.ipify.org/ 
password=YOUR_API_TOKEN
zone=example.com
vpn.example.com
```


After making the necessary changes to the configuration file, restart the `ddclient` service in order for the changes to take effect.

```bash
sudo systemctl restart ddclient.service
```

We can test our configuration by running the following command.

```bash
sudo ddclient -debug -verbose -noquiet
```

Somewhere near the bottom, you should see a line like this.

```
SUCCESS:  updating vpn.example.com: IPv4 address set to 255.255.255.255
```

