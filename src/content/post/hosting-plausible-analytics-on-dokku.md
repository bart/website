---
publishDate: 2023-04-23T00:00:00Z
title: Hosting Plausible Analytics on Dokku
excerpt: Plausible is an application that provides web analytics. These are the instructions on how to install Plausible on Dokku.
tags:
  - self-hosting
  - plausible
  - dokku
---

Plausible is an application that provides web analytics but actually respects your users privacy. Below are the instructions on how to install Plausible on Dokku.

This is a modified version of the work done by [Kevin Langley Jr](https://kevinlangleyjr.dev/blog/self-hosting-plausible-analytics-with-dokku). I made some modifications to the article and upgraded the Dockerfile to the latest Plausible version.

## Installation

First, you need to SSH into your Dokku Host and create the Plausible app:

```bash
dokku apps:create plausible
```

Next, you need to install the PostgreSQL and Clickhouse plugins, if they aren't already installed.

```bash
dokku plugin:install https://github.com/dokku/dokku-postgres.git postgres
dokku plugin:install https://github.com/dokku/dokku-clickhouse.git clickhouse
```

Create the instances of the PostgresSQL and Clickhouse databases.

```bash
dokku postgres:create plausible-db
dokku clickhouse:create plausible-events-db
```

Then set the environment variables for the database scheme, which just sets it to use the scheme expected within the Plausible Docker image. Then link the databases to the application.

```bash
dokku config:set plausible CLICKHOUSE_DATABASE_SCHEME=http
dokku postgres:link plausible-db plausible
dokku clickhouse:link plausible-events-db plausible
```

Next, you'll slightly adjust the URL that was provided from linking the Clickhouse database and change the name of the environment variable for it as well to match what is expected within the Plausible docker image.

```bash
dokku config:set plausible CLICKHOUSE_DATABASE_URL=$(dokku config:get plausible CLICKHOUSE_URL)/plausible
dokku config:unset plausible CLICKHOUSE_URL
```

Then you need to generate a secrete key using `openssl`.

```bash
dokku config:set plausible SECRET_KEY_BASE=$(openssl rand -base64 64 | tr -d '\n')
```

And then, we'll set the `BASE_URL` environment variable and set the domain for the application — make sure to use your domain here.

```bash
dokku config:set plausible BASE_URL=https://analytics.example.com
dokku domains:set plausible analytics.example.com
```

Set the required SMTP environment variables to set up transactional emails.

```bash
dokku config:set plausible MAILER_EMAIL=admin@example.com \
                           SMTP_HOST_ADDR=mail.example.com \
                           SMTP_HOST_PORT=465 \
                           SMTP_USER_NAME=admin@example.com \
                           SMTP_USER_PWD=password \
                           SMTP_HOST_SSL_ENABLED=true
```


Set up the admin account for the application. And then, we can optionally disable registration, this is especially useful if this is just for you and you aren't inviting any clients or other users to utilize it.

```bash
dokku config:set plausible ADMIN_USER_EMAIL=admin@example.com \
                           ADMIN_USER_NAME=admin \
                           ADMIN_USER_PWD=password

dokku config:set plausible DISABLE_REGISTRATION=true
```

Next, you need to clone the repository on your local machine.

```bash
git clone https://codeberg.org/bart/dokku-plausible.git
```

Set the Dokku host as the git remote for the repository and push it on up to your Dokku host.git remote add dokku dokku@dokku.example.com:plausible

```bash
git push dokku master
```

Finally, you can generate the Let's Encrypt SSL certificates.

```bash
dokku letsencrypt:enable plausible
```

You can now access your Plausible instance via the domain you configured.

