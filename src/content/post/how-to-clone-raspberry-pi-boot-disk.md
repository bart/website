---
publishDate: 2024-02-22T15:00:00Z
title: How to clone Raspberry Pi boot disk
excerpt: These are the instructions on how to clone your Raspberry Pi boot disk to an external drive.
tags:
  - raspberrypi
  - rpi-clone
---

I recently installed my [Raspberry Pi rack mount](https://www.uctronics.com/raspberry-pi/1u-rack-mount/uctronics-pi-rack-pro-for-raspberry-pi-4b-19-1u-rack-mount-support-for-4-2-5-ssds.html) by UCTRONICS. The mount provides the possibility to connect a SSD to the Raspberry Pi over USB. Since my Raspberry Pi was already running from the SD card, I needed to clone my boot disk over to the SSD.

Previously, I tried using the `rpi-clone` tool, but ran into some problems that only seemed to exist on the newer Raspberry Pi OS versions. But more recently, I discovered [this fork](https://rpi-clone.jeffgeerling.com/) by Jeff Geerling that fixed all the problems I was experiencing.

You can install the tool using the command below.

```bash
curl https://raw.githubusercontent.com/geerlingguy/rpi-clone/master/install | sudo bash
```

Next, you need to clone the SD card to the connected SSD.

```bash
sudo rpi-clone sda
```

Finally, shutdown the Raspberry Pi, remove the SD card and power the Pi back on again. Your system should now boot from the SSD.

