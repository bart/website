---
publishDate: 2023-09-21T00:00:00Z
title: How to connect front panel connectors
excerpt: Use this diagram to connect the front panel connectors to the motherboard.
tags:
  - hardware
  - self-hosting
  - homelab
---

Recently, I built my own NAS server, which was my first time building a computer. This diagram helped me connect the front panel connectors to the motherboard. For future reference, I am sharing the diagram here.

![Front panel connectors diagram](~/assets/images/front-panel-connectors.jpg)

