---
publishDate: 2023-04-23T00:00:00Z
title: Initial Server Setup on Ubuntu
excerpt: Before I use a new Ubuntu server for anything else, these are some important configuration steps I follow.
tags:
  - linux
  - fail2ban
  - unattended-upgrades
---

Before I use a new Ubuntu server for anything else, these are some important configuration steps I follow. 

## Creating a New User

After logging in as root, you need to create a new user account. In the future, we'll log in with this new account.

```bash
adduser bart
```

You will be asked a few questions, starting with the account password. Fill in any additional information if you want, this information is not required.
Now that you have a new user account, we need to grant the new user account administrative privileges.

```bash
usermod -aG sudo bart
```

Finally, we need to copy the root user's .ssh directory to the new user account's home directory. This allows you to log in using the new user account using SSH.

```bash
rsync --archive --chown=bart:bart ~/.ssh /home/bart
```

Now, open up a new terminal session, and use SSH to connect to the server with your new user account. 

## Protect SSH with Fail2ban

Fail2ban is a tool that automatically creates firewall rules to ban specific IP addresses after a certain number of failed login attempts. This will help you protect your server from brute force attacks.

Fail2ban is available in Ubuntu's software repositories. Begin with running the following commands to update your package listings and install the Fail2ban package.

```bash
sudo apt update
sudo apt install fail2ban
```

Fail2ban keeps its configuration files in the `/etc/fail2ban` directory. 

```bash
cd /etc/fail2ban
```

Before making configuration changes, we need to copy the default configuration file. Whenever Fail2ban is updated, the default configuration file can be updated. It is used for any default settings that you have not created overrides for.

```bash
sudo cp jail.conf jail.local
```

We can now begin making changes to the configuration. Open the configuration file.

```bash
sudo vim jail.local
```

We can now enable Fail2ban for the SSH service. We do this by adding the following line to the `[sshd]` section.

```
[sshd]
...
enabled = true
...
```

Finally, after finishing editing, you need to enable and start the Fail2ban service.

```bash
sudo systemctl enable fail2ban
sudo systemctl start fail2ban
```

## Configuring Unattended Upgrades

Ubuntu provides a tool called `unattended-upgrades` in order to automatically install security patches and other upgrades for your server.

If this tool is not automatically installed, you can install it with the following commands.

```bash
sudo apt update
sudo apt install unattended-upgrades
```

You can now configure the tool by changing the configuration file. Open this file using the editor.

```bash
sudo vim /etc/apt/apt.conf.d/50unattended-upgrades
```

If you made changes to the configuration file, restart the `unattended-upgrades` service in order for the changes to take effect.

```bash
sudo systemctl restart unattended-upgrades.service
```

