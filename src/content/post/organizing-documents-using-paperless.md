---
publishDate: 2023-11-17T00:00:00Z
title: Organizing documents using Paperless
excerpt: Here is how I organize my documents using Paperless-ngx.
tags:
  - self-hosting
  - paperless
---

I have been playing around with the idea of using Paperless-ngx for some time, but every time I tried it, I reverted to using folders. But it kept me interested, so I tried it again. I started by reading about other people's experiences and how they use Paperless to manage their documents. This provided me with a good starting point for organizing my own documents. Finally, I came up with the following system.

1. Document Types are the broadest types in the system. As the name suggests, these refer to the type of document. In my case, these documents include invoices, certificates, manuals, and contracts.
2. Correspondents refers to the person or organization that you are communicating with in the document. Examples of this are my employer, my bank, or a web shop.
3. Tags are used to indicate what the communication is about. Tags can be classified as car, medical, or based on the product in the manual. You can add multiple tags to a single document, which is one of the benefits of going paperless, compared to having to use a folder structure and documents only being able to relate to a single topic.

There is no doubt this system will change in the future, but this is what works for me at the moment.

