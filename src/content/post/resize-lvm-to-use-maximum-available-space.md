---
publishDate: 2023-05-05T00:00:00Z
title: Resize LVM to use maximum available space
excerpt: Resize the default Ubuntu LVM root volume to use the maximum available space.
tags:
  - linux
  - ubuntu
  - lvm
---

When you set up Ubuntu Server using the installer, Ubuntu sets the default root volume size to 100 GB. I realized this after I had already configured Ubuntu, so I needed to fix this mistake while the server was running.

First, you need to tell LVM to extend the volume to the maximum size available.

```bash
sudo lvextend -l +100%FREE /dev/ubuntu-vg/ubuntu-lv
```

Finally, you need to resize the root file system.

```bash
sudo resize2fs /dev/mapper/ubuntu--vg-ubuntu--lv
```

Your volume has now been resized to use the maximum available space.

