import { getAsset, getPermalink } from './utils/permalinks';

export const headerData = {
  links: [
    {
      text: 'Home',
      href: getPermalink('/')
    },
    {
      text: 'About',
      href: getPermalink('/about')
    },
    {
      text: 'Contact',
      href: getPermalink('/contact')
    }
  ],
  actions: [],
};

export const footerData = {
  socialLinks: [
    { ariaLabel: 'Keyoxide', icon: 'ic:round-verified', href: 'https://keyoxide.org/bart@bartmathijssen.com' },
    { ariaLabel: 'Codeberg', icon: 'simple-icons:codeberg', href: 'https://codeberg.org/bart' },
    { ariaLabel: 'GoToSocial', icon: 'simple-icons:activitypub', href: 'https://social.bartmathijssen.com/@bart', rel: 'me' },
    { ariaLabel: 'Matrix', icon: 'simple-icons:matrix', href: 'https://matrix.to/#/@bart:bartmathijssen.com' },
    { ariaLabel: 'RSS', icon: 'tabler:rss', href: getAsset('/rss.xml') }
  ],
  footNote: `
    Content available under <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>.
    Design based on <a href="https://github.com/onwidget/astrowind">AstroWind</a>.
  `,
};
