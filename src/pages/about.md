---
title: 'About Me'
layout: '~/layouts/MarkdownLayout.astro'
---

My name is Bart Mathijssen, I am a freelance software developer from Tilburg, The Netherlands. 

Currently, I am a volunteer at [CoderDojo Tilburg](https://code013.nl), teaching the new generation about technology and maintaining their website and other systems. I am also involved in the organization of [FOSDEM Junior](https://fosdem.org/2024/schedule/track/junior/).

I am a free software and privacy advocate, and spend a lot of my time in the open-source space. Because of my strong beliefs about these topics, I try to be an active member of the community. I do this by not only contributing source code but also by representing projects, answering questions in the forums, or taking part in community activities. This is one of the reasons I am an active member of the [Codeberg e.V.](https://codeberg.org/) non-profit organization. I think Codeberg is a great example of an organization that represents my personal beliefs.

Previously, I worked as a web developer at [Tambien](https://tambien.nl), where I developed web applications for the manufacturing industry. These applications included webshops, landing pages, planning tools, and intranets. A selection of projects I have worked on is available on the [projects](/projects/) page. The variety of these applications has given me a skill set that covers various tools and technologies.

If you have any questions or simply want to say hello, please feel free to contact me by sending an email to bart@bartmathijssen.com or by choosing one of the other options listed on the [contact](/contact/) page.

