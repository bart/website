---
title: 'Contact Me'
layout: '~/layouts/MarkdownLayout.astro'
---

Email is the easiest way to get in touch with me. I also host my own Synapse server.

The only social network I am a member of is the Fediverse. My account lives on a self-hosted Mastodon instance.

* **Email:** bart@bartmathijssen.com 
* **Matrix:** [@bart:bartmathijssen.com](https://matrix.to/#/@bart:bartmathijssen.com)
* **Mastodon:** [@bart@bartmathijssen.com](https://mastodon.bartmathijssen.com/@bart)
* **Codeberg:** [codeberg.org/bart](https://codeberg.org/bart)

I am a company registered with the Dutch Chamber of Commerce. My registration number is 92367941. My VAT ID number is NL004951005B15.

