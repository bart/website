---
title: 'All Projects'
layout: '~/layouts/MarkdownLayout.astro'
---

Here are some of the projects I've worked on over the years. Some projects are not listed because they are internal applications or because I only contributed a small part.

| Year | Project                           | Made at | Link                                                             |
| :--- | :-------------------------------- | :------ | :--------------------------------------------------------------- |
| 2023 | Tobroco Portal V2                 | Tambien |                                                                  |
| 2023 | Iveco Schouten Onboarding Tool    | Tambien |                                                                  |
| 2023 | Tobroco Telemetry                 | Tambien |                                                                  |
| 2023 | Oxbo BOMCheck                     | Tambien |                                                                  |
| 2023 | Tobroco Koppeling Isah Sontheim   | Tambien |                                                                  |
| 2022 | Tobroco Mobile Logistics          | Tambien |                                                                  |
| 2022 | Tambien Mobile Logistics          | Tambien |                                                                  |
| 2022 | Tambien Kennisbank                | Tambien |                                                                  |
| 2022 | Van Vuuren Portal                 | Tambien |                                                                  |
| 2022 | CoderDojo Tilburg Website         |         | [code013.nl](https://code013.nl)                                 |
| 2022 | Lightronics Webshop               | Tambien |                                                                  |
| 2021 | Textiellab Sample Studio          | Tambien | [samplestudio.textiellab.nl](https://samplestudio.textiellab.nl) |
| 2021 | Iveco Schouten Urenverantwoording | Tambien |                                                                  |
| 2021 | Peter Mathijssen Website          |         | [petermathijssen.nl](https://petermathijssen.nl)                 |
| 2021 | HANSA-FLEX Productieportaal       | Tambien |                                                                  |
| 2020 | Van Pelt Hengelsport V2           | Tambien | [vanpelthengelsport.nl](https://vanpelthengelsport.nl)           |
| 2020 | Pittler Service App               | Tambien |                                                                  |
| 2020 | Werken bij Dufec                  | Tambien | [werkenbijdufec.nl](https://werkenbijdufec.nl)                   |
| 2020 | Dufec Website                     | Tambien | [dufec.nl](https://dufec.nl)                                     |
| 2020 | Kodular Creator                   | Kodular | [creator.kodular.io](https://creator.kodular.io)                 |
| 2020 | Mijn Cromvoirtse                  | Tambien |                                                                  |
